import {Pagination} from '@shopify/polaris';
import React from 'react';
import './PaginationCustom.scss';
export const PaginationCustom = () => {
  return (
    <div className="pagination">
      <Pagination
        hasPrevious
        onPrevious={() => {
          console.log('Previous');
        }}
        hasNext
        onNext={() => {
          console.log('Next');
        }}
      />
    </div>
  );
};

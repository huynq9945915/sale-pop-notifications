import React from 'react';
import {FormLayout, Layout, Select, TextField} from '@shopify/polaris';
import {useState, useCallback} from 'react';
// eslint-disable-next-line react/prop-types
export default function Triggers(input, handleChangeInput) {
  const inputData = input.input;

  const options = [
    {label: 'All pages', value: 'all'},
    {label: 'Some pages', value: 'specific'}
  ];

  return (
    <FormLayout>
      <Layout>
        <Layout.Section fullWidth>
          <Select
            options={options}
            onChange={handleChangeInput}
            value={inputData.allowShow}
            label={''}
          />
        </Layout.Section>
        <Layout.Section fullWidth>
          {inputData.allowShow === 'all' ? (
            <TextField
              label="Excluded pages"
              value={inputData.allowShow}
              onChange={handleChangeInput}
              multiline={4}
              autoComplete="off"
              helpText={
                'Pages URLs NOT to show the pop-up (separated by new lines)'
              }
            />
          ) : (
            <>
              <TextField
                label="Included pages"
                value={inputData.allowShow}
                onChange={handleChangeInput}
                multiline={4}
                autoComplete="off"
                helpText={
                  'Pages URLs to show the pop-up (separated by new lines)'
                }
              />
              <br />
              <TextField
                label="Excluded pages"
                value={inputData.allowShow}
                onChange={handleChangeInput}
                multiline={4}
                autoComplete="off"
                helpText={
                  'Pages URLs NOT to show the pop-up (separated by new lines)'
                }
              />
            </>
          )}
        </Layout.Section>
      </Layout>
    </FormLayout>
  );
}

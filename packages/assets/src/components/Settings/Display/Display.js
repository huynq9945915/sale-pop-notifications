import React, {useState, useCallback} from 'react';
import {Checkbox, FormLayout, Layout} from '@shopify/polaris';
import DesktopPositionInput from './DesktopInputPosition';
import Timing from './Timing';
// eslint-disable-next-line react/prop-types
function Display(input, handleChangeInput) {
  const inputData = input.input;
  console.log(inputData);
  const [hideTime, setHideTime] = useState(inputData.hideTimeAgo);
  const [truncate, setTruncate] = useState(inputData.truncateProductName);
  // const [displayDuration, setDisplayDuration] = useState(
  //   inputData.displayDuration
  // );
  // const [timeBefore, setTimeBefore] = useState(inputData.firstDelay);
  // const [gapTime, setGapTime] = useState(inputData.popsInterval);
  // const [maxPop, setMaxPop] = useState(inputData.maxPopsDisplay);
  //
  // const handleDisplayChange = useCallback(
  //   value => setDisplayDuration(value),
  //   []
  // );
  // const handleTimeBeforeChange = useCallback(value => setTimeBefore(value), []);
  // const handleChangePosition = useCallback(value => {
  //   setPosition(value);
  // }, []);
  // const handleGapTimeChange = useCallback(value => setGapTime(value), []);
  // const handleMaxPopChange = useCallback(value => setMaxPop(value), []);
  //
  // const handleHideTimeChange = useCallback(value => setHideTime(value), []);
  // const handleTruncateChange = useCallback(value => setTruncate(value), []);

  return (
    <FormLayout>
      <DesktopPositionInput
        label={'Desktop Position'}
        value={inputData}
        onChange={handleChangeInput}
        helpText={'The display position of the pop on your website'}
      />
      <Checkbox
        label="Hide time ago"
        checked={hideTime}
        onChange={async () => {
          await setHideTime(hideTime);
          handleChangeInput('hideTimeAgo', hideTime);
        }}
      />
      <Checkbox
        label="Truncate content text"
        checked={truncate}
        onChange={async () => {
          await setTruncate(truncate);
          handleChangeInput('truncateProductName', truncate);
        }}
        helpText={
          "If your product name is long for one line, it will be truncate to  'Product na...'"
        }
      />
      <Layout>
        <Layout.Section secondary>
          <Timing
            label={'Display duration'}
            value={inputData}
            onChange={handleChangeInput}
            helpText={'How long each pop will display on your page'}
          />
          <Timing
            label={'Gap time between two pops'}
            value={inputData}
            onChange={handleChangeInput}
            helpText={'The time interval between two popup notifications'}
          />
        </Layout.Section>
        <Layout.Section secondary>
          <Timing
            label={'Time before the first pop'}
            value={inputData}
            onChange={handleChangeInput}
            helpText={'The delay time the first notification'}
          />
          <Timing
            label={'Maximum of popups'}
            value={inputData}
            onChange={handleChangeInput}
            helpText={
              'The maximum number of popups are allowed to show after' +
              ' page loading. Maximum number is 80'
            }
          />
        </Layout.Section>
      </Layout>
    </FormLayout>
  );
}

export default Display;

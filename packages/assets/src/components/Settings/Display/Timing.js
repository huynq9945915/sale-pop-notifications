import React from 'react';
import {Layout, RangeSlider, Stack} from '@shopify/polaris';

// eslint-disable-next-line react/prop-types
const Timing = (label, value, onChange, helpText) => {
  return (
    <Layout.Section oneHalf>
      <Stack>
        <Stack.Item fill>
          <RangeSlider label={label} value={value} onChange={onChange} />
        </Stack.Item>
        <Stack.Item>
          <div
            style={{
              padding: '8px 5px',
              border: '1px solid #333',
              borderRadius: '5px',
              marginTop: '15px'
            }}
          >
            <p>
              {' '}
              {value    } <span style={{opacity: '0.6'}}> second(s)</span>
            </p>
          </div>
        </Stack.Item>
        <Stack.Item>
          <span style={{opacity: '0.6', marginTop: '15px'}}> {helpText}</span>
        </Stack.Item>
      </Stack>
    </Layout.Section>
  );
};

export default Timing;

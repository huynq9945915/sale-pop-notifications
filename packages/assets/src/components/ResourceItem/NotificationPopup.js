import React from 'react';
import './NotificationPopup.scss';
export const NotificationPopup = ({
  // eslint-disable-next-line react/prop-types
  firstName = 'John Doe',
  // eslint-disable-next-line react/prop-types
  city = 'Ha Noi',
  // eslint-disable-next-line react/prop-types
  country = 'Viet Nam',
  // eslint-disable-next-line react/prop-types
  productName = 'Laptop',
  // eslint-disable-next-line react/prop-types
  timestamp = 'a day ago',
  // eslint-disable-next-line react/prop-types
  productImage = 'https://tinyurl.com/laptop-image-g0su',
  // eslint-disable-next-line react/prop-types
  settings = {hideTimeAgo: false, truncateProductName: true}
}) => {
  function truncateString(str, num) {
    if (str.length > num) {
      return str.slice(0, num) + '...';
    } else {
      return str;
    }
  }
  const {hideTimeAgo, truncateProductName} = settings;
  return (
    <div className="Avava-SP__Wrapper fadeInUp animated">
      <div className="Avava-SP__Inner">
        <div className="Avava-SP__Container">
          <a href="#" className={'Avava-SP__LinkWrapper'}>
            <div
              className="Avava-SP__Image"
              style={{
                backgroundImage: `url(${productImage})`
              }}
            ></div>
            <div className="Avada-SP__Content">
              <div className={'Avada-SP__Title'}>
                {firstName} in {city}, {country}
              </div>
              <div className={'Avada-SP__Subtitle'}>
                <b>
                  Purchased{' '}
                  {truncateProductName
                    ? truncateString(productName, 16)
                    : productName}
                </b>
              </div>
              <div className={'Avada-SP__Footer'}>
                {hideTimeAgo ? '' : timestamp}{' '}
                <span className="uni-blue">
                  <i className="fa fa-check" /> by Avada
                </span>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};

import React from 'react';
import './NotificationPopup.scss';
import {ResourceItem, ResourceList, Stack, TextStyle} from '@shopify/polaris';
import {NotificationPopup} from './NotificationPopup';
/**
 * Render sample loading page
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function RenderItem({
  selectedItems,
  setSelectedItems,
  sortValue,
  setSortValue,
  resourceName,
  data,
  setData
}) {
  return (
    <ResourceList
      resourceName={resourceName}
      items={data}
      selectedItems={selectedItems}
      onSelectionChange={setSelectedItems}
      selectable
      sortValue={sortValue}
      sortOptions={[
        {label: 'Newest update', value: 'DATE_MODIFIED_DESC'},
        {label: 'Oldest update', value: 'DATE_MODIFIED_ASC'}
      ]}
      onSortChange={selected => {
        setSortValue(selected);
        console.log(`Sort option changed to ${selected}.`);
      }}
      renderItem={item => {
        const {
          id,
          firstName,
          city,
          country,
          productName,
          timestamp,
          productImage
        } = item;
        return (
          <ResourceItem id={id}>
            <Stack>
              <Stack.Item fill>
                <NotificationPopup
                  firstName={firstName}
                  city={city}
                  country={country}
                  productName={productName}
                  timestamp={timestamp}
                  productImage={productImage}
                />
              </Stack.Item>
              <Stack.Item>
                <TextStyle>From March 8, 2021</TextStyle>
              </Stack.Item>
            </Stack>
          </ResourceItem>
        );
      }}
    />
  );
}

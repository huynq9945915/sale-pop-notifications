import React, {useCallback, useState} from 'react';
import {AccountConnection, Layout, Page} from '@shopify/polaris';
/**
 * Render a home page for overview
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function Home() {
  const [connected, setConnected] = useState(false);

  const handleAction = useCallback(() => {
    setConnected(connected => !connected);
  }, []);

  const buttonText = connected ? 'Disconnect' : 'Connect';
  const title = `App status is ${connected ? 'Disconnect' : 'Connect'}`;
  return (
    <Page title="Home" fullWidth>
      <Layout>
        <Layout.Section>
          <AccountConnection
            title={title}
            action={{
              content: buttonText,
              onAction: handleAction
            }}
          />
        </Layout.Section>
      </Layout>
    </Page>
  );
}

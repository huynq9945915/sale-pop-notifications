import React from 'react';
import {Layout, Page, Card, Spinner} from '@shopify/polaris';
import {useState} from 'react';
import RenderItem from '../../components/ResourceItem';
import {PaginationCustom} from '../../components/Pagination/PaginationCustom';
import useFetchApi from '../../hooks/api/useFetchApi';
/**
 * Just render a sample page
 *
 * @param {Function} paginateSamples
 * @param {Object} sample
 * @return {React.ReactElement}
 * @constructor
 */
export default function Notifications() {
  const [selectedItems, setSelectedItems] = useState([]);
  const [sortValue, setSortValue] = useState('DATE_MODIFIED_DESC');
  const resourceName = {
    singular: 'notification',
    plural: 'notifications'
  };
  const {
    loading,
    data: notifications,
    setData: setNotifications,
    setLoading
  } = useFetchApi('/notifications');
  return (
    <Page
      title="Notifications"
      subtitle="List of sales notifcation from Shopify"
      fullWidth
    >
      {loading ? (
        <Spinner accessibilityLabel="Spinner example" size="large" />
      ) : (
        <Layout>
          <Layout.Section>
            <Card>
              <RenderItem
                selectedItems={selectedItems}
                setSelectedItems={setSelectedItems}
                sortValue={sortValue}
                setSortValue={setSortValue}
                resourceName={resourceName}
                data={notifications}
                setData={setNotifications}
              />
            </Card>
            <PaginationCustom />
          </Layout.Section>
        </Layout>
      )}
    </Page>
  );
}

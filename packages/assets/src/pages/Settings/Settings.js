import React from 'react';
import {
  Layout,
  Page,
  Button,
  Card,
  Tabs,
  Form,
  Spinner
} from '@shopify/polaris';
import {NotificationPopup} from '../../components/ResourceItem/NotificationPopup';
import {useState, useCallback} from 'react';
import Triggers from '../../components/Settings/Triggers/Triggers';
import Display from '../../components/Settings/Display/Display';
import useFetchApi from '../../hooks/api/useFetchApi';
import defaultSettings from '../../const/settings/defaultSettings';
/**
 * Just render a sample page
 *
 * @param {Function} paginateSamples
 * @param {Object} sample
 * @return {React.ReactElement}
 * @constructor
 */
export default function Settings() {
  const [selected, setSelected] = useState(0);
  const {loading, data: input, setData: setInput, setLoading} = useFetchApi(
    '/settings',
    defaultSettings
  );
  // const [hideTime, setHideTime] = useState(input.hideTimeAgo);
  // const [truncate, setTruncate] = useState(input.truncateProductName);
  // const [position, setPosition] = useState(input.position);
  // const [displayDuration, setDisplayDuration] = useState(input.displayDuration);
  // const [timeBefore, setTimeBefore] = useState(input.firstDelay);
  // const [gapTime, setGapTime] = useState(input.popsInterval);
  // const [maxPop, setMaxPop] = useState(input.maxPopsDisplay);
  //
  // const [allShow, setAllShow] = useState(input.allowShow);
  // const [includedUrls, setIncludedUrls] = useState('');
  // const [excludedUrls, setExcludedUrls] = useState('');

  const handleTabChange = useCallback(
    selectedTabIndex => setSelected(selectedTabIndex),
    []
  );
  const handleChangeInput = (key, value) => {
    setInput(prevInput => ({
      ...prevInput,
      [key]: value
    }));
  };
  console.log(input);
  const tabs = [
    {
      id: 'display-tab',
      content: 'Display',
      subContent: 'APPEARANCE',
      accessibilityLabel: 'All customers',
      panelID: 'all-customers-content-1',
      body: <Display input={input} handleChangeInput={handleChangeInput} />
    },
    {
      id: 'trigger-tab',
      content: 'Triggers',
      subContent: 'PAGES RESTRICTION',
      panelID: 'accepts-marketing-content-1',
      body: <Triggers input={input} handleChangeInput={handleChangeInput} />
    }
  ];
  return (
    <Page
      title="Settings"
      primaryAction={<Button primary>Save</Button>}
      subtitle={'Decide how your notifications will display'}
      fullWidth
    >
      {loading ? (
        <Spinner accessibilityLabel="Spinner example" size="large" />
      ) : (
        <Layout>
          <Layout.Section secondary>
            <NotificationPopup />
          </Layout.Section>
          <Layout.Section>
            <Card sectioned>
              <Form>
                <Tabs
                  tabs={tabs}
                  selected={selected}
                  onSelect={handleTabChange}
                >
                  <Card.Section title={tabs[selected].subContent}>
                    {tabs[selected].body}
                  </Card.Section>
                </Tabs>
              </Form>
            </Card>
          </Layout.Section>
        </Layout>
      )}
    </Page>
  );
}

import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from '../pages/Home';
import Notifications from '../pages/Notifications';
import Settings from '../pages/Settings';
import NotFound from '../pages/NotFound';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/notifications" component={Notifications} />
    <Route exact path="/settings" component={Settings} />
    <Route path="*" component={NotFound} />
  </Switch>
);

export default Routes;

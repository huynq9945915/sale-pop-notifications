import * as functions from 'firebase-functions';

const {app} = functions.config();

export default {
  baseUrl: app.baseurl,
  isProduction: app.env === 'production'
};

import {getSettings, updateSettings} from '../repositories/settingsRepository';
import {getCurrentUserInstance} from '../helpers/auth';

export async function get(ctx) {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const settings = await getSettings(shopID);
    return (ctx.body = {
      data: settings,
      success: true
    });
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      error: e.message
    };
  }
}

export async function update(ctx) {
  try {
    const inputData = ctx.req.body;
    const {shopID} = getCurrentUserInstance(ctx);
    await updateSettings(shopID, inputData);
    ctx.status = 201;
    return (ctx.body = {
      success: true
    });
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      error: e.message
    };
  }
}

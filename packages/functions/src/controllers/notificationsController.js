import {getNotifications} from '../repositories/notificationRepository';

export async function get(ctx) {
  try {
    const notifications = await getNotifications(ctx);
    return (ctx.body = {
      data: notifications,
      success: true
    });
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      error: e.message
    };
  }
}

import Shopify from 'shopify-api-node';
import {Firestore} from '@google-cloud/firestore';

const firestore = new Firestore();
const shopRef = firestore.collection('shops');

export async function shopifyService(ctx) {
  const domain = ctx.state.user.shop.domain;
  const docsRef = await shopRef
    .where('domain', '==', domain)
    .limit(1)
    .get();
  if (docsRef.empty) {
    return null;
  }
  const doc = docsRef.docs[0];
  return new Shopify({
    shopName: doc.data().name,
    accessToken: doc.data().accessToken
  });
}

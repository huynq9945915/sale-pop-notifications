import {database} from './firebaseInit';
import {Firestore} from '@google-cloud/firestore';
import formatDateFields from '../helpers/formatDateFields';

const firestore = new Firestore();
const collection = firestore.collection('settings');
const settingsRef = database.collection('settings');

export async function getSettings(shopID) {
  const docsRef = await collection
    .where('shopID', '==', shopID)
    .limit(1)
    .get();
  if (docsRef.empty) {
    return null;
  }
  const doc = docsRef.docs[0];
  return {...formatDateFields(doc.data()), id: doc.id};
}

export async function updateSettings(shopID, newData) {
  const snapshot = await settingsRef.get();
  const convert = snapshot.docs.map(doc => ({id: doc.id, ...doc.data()}));
  const data = convert.filter(doc => {
    return doc.shopID === shopID;
  });
  if (data) {
    return await settingsRef.doc(data[0].id).update(newData);
  }
  return await settingsRef.add(newData);
}

import * as admin from 'firebase-admin';
const serviceAccount = require('../../serviceAccount.development.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});
export const database = admin.firestore();

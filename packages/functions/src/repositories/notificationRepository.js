import formatDateFields from '../helpers/formatDateFields';
import {Firestore} from '@google-cloud/firestore';
import {getOrderFromStore} from './ordersRepository';

const firestore = new Firestore();
const collection = firestore.collection('notifications');

export async function getNotifications(ctx) {
  await addOrderToNotification(ctx);
  const snapshot = await collection.get();
  return snapshot.docs.map(doc => ({
    id: doc.id,
    ...formatDateFields(doc.data())
  }));
}

async function addOrderToNotification(ctx) {
  collection.listDocuments().then(val => {
    val.map(val => {
      val.delete();
    });
  });
  const orders = await getOrderFromStore(ctx);
  for (const order of orders) {
    await collection.add(order);
  }
}

import {getCurrentUserInstance} from '../helpers/auth';
import {shopifyService} from '../services/shopifyServices';

export async function getOrderFromStore(ctx) {
  const domain = ctx.state.user.shop.domain;
  const shopify = await shopifyService(ctx);
  const orders = await shopify.order.list({limit: 30});
  const productIdInOrders = Array.from(
    new Set(orders.map(order => order.line_items[0].product_id))
  );
  const productImgs = (await shopify.product.list())
    .map(product => {
      if (productIdInOrders.find(productId => product.id === productId)) {
        return {
          id: product.id,
          image: product.images[0].src
        };
      }
    })
    .filter(Boolean);
  return orders.map(order => {
    const productID = order.line_items[0].product_id;
    const productImg = productImgs.find(
      productImg => productImg.id === productID
    ).image;
    return {
      orderId: order.id,
      city: order.billing_address.city,
      country: order.billing_address.country,
      firstname: order.customer.first_name,
      domain: domain,
      productImage: productImg,
      productId: productID,
      productName: order.line_items.map(item => item.name).join(','),
      shopID: getCurrentUserInstance(ctx).shopID,
      timestamp: order.created_at
    };
  });
}

export async function saveOrder(ctx) {
  // const docsRef = await ordersRef
}
